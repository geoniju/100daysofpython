
Should_Continue = True
#TODO-1: Combine the encrypt() and decrypt() functions into a single function called caesar().

def caesar(start_text, shift_amount, cypher_direction):
    end_text = ""
    if cypher_direction == 'decode':
        shift_amount *= -1
    for char in start_text:
        position = alphabet.index(char)
        new_position = position + shift_amount
        print(new_position)
        end_text += alphabet[new_position]

    print(f"The {cypher_direction}d text is {end_text}")

while Should_Continue:
    alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
                'v', 'w', 'x', 'y', 'z']

    direction = input("Type 'encode' to encrypt, type 'decode' to decrypt:\n")
    text = input("Type your message:\n").lower()
    shift = int(input("Type the shift number:\n"))
    caesar(text, shift, direction)

    result = input('Do you want to continue?')
    if result == 'Yes':
        Should_Continue = True
    else:
        Should_Continue = False
def add(n1, n2):
    return n1 + n2


def subtract(n1, n2):
    return n1 - n2


def multiply(n1, n2):
    return n1 * n2


def divide(n1, n2):
    return n1 / n2


# create a dictionary with symbol as key and function name as values

operations = {
    '+': add,
    '-': subtract,
    '*': multiply,
    '/': divide
}
num1 = int(input("What is the first number: "))
for symbol in operations:
    print(symbol)
operation_symbol = input("Pick an operation from the line above")
cont = "y"
while cont == 'y':

    num2 = int(input("What is the next number: "))
    result_fn = operations[operation_symbol]
    answer = result_fn(num1, num2)

    print(f"{num1} {operation_symbol} {num2}  =  {answer}")

    num1 = answer

    cont = input(f" Type 'y' to continue calculating with {answer} or type 'n' to exit :")
    for symbol in operations:
        print(symbol)
    operation_symbol = input("Pick another operation from the line above")



bid_dict = {}
want_bid = True
while want_bid:
    name = input("What is your name?")
    bid = int(input("What is your bid amount?"))
    bid_dict[name] = bid
    want_bid = input("if there are other users who wants to bid?")
    if want_bid == 'yes':
        want_bid = True
    else:
        want_bid = False

max_val = 0
max_key = 0
for key, value in bid_dict.items():

    if value > max_val:
        max_val = value
        max_key = key


print(f"The winner is {max_key} with a bid of {max_val}")